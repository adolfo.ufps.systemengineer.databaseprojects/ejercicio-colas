package Vista;

import Util.seed.Cola;

public class MatrizCola {

    private Cola<Integer>[] c;

    @SuppressWarnings("unchecked")
    public MatrizCola(int[][] m) {
        if (m != null) {

            this.c = new Cola[m.length];

            for (int i = 0; i < this.c.length; i++) {
                c[i] = new Cola<Integer>();
            }
            for(int i = 0; i < this.c.length; i++){
                for(int j = 0; j < c[i].size(); j++){
                    c[i].enColar(m[i][j]);
                }
            }
        } else {
            throw new RuntimeException("¡LA COLA ES NULA! ¡AHHHH! ¡SANGRE! ¡MUCHA SANGRE!\nY...Muerte...");
        }
    }

    public int get(int i, int j) {
        int number = 0;
        if (this != null) {
            for(int x = 0; x <= i; x++){
                Cola<Integer> a = c[x];
                for(int y = 0; y <= j; y++){
                   if(y == j){
                    return c[x].deColar();
                   } 
                }
            }
        }
        return number;
    }

    // El vector de colas existe
    // En esa posición, existe la cola en esa posición

    public void set(int i, int j, int obj) {

    }

    public Cola<Integer> getMenores() {
        return null;
    }

    @Override
    public String toString() {
        String msg = "\n";
        for (int i = 0; i < this.c.length; i++) {
            Cola<Integer> x = new Cola<Integer>();
            x = c[i];
            for (int j = 0; j < c[i].size(); j++) {
                msg = msg + "\t" + x.deColar();
            }
            msg = msg + "\n";
        }
        return msg;
    }
}
