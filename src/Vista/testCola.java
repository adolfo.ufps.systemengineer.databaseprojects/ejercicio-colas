package Vista;

public class testCola {
    public static void main(String[] args) {

        int[][] m = new int[5][5];
        MatrizCola matriz = null;

        String msg = "\n";

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                m[i][j] = j;
                msg = msg + "\t" + m[i][j];
            }
            msg = msg + "\n";
        }

        System.out.println(msg);

        System.out.println("******************************************");

        try {
            matriz = new MatrizCola(m);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
